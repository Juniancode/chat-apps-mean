// Joi adalah liblary node untuk validator tipe data seperti email.string dll
const Joi = require('joi');
// HTtpStatus code adalah liblary status dari sebuah website
const HttpStatus = require('http-status-codes');
// Bycrtypt js liblary untuk encode data
const bcrypt = require('bcryptjs');
// JWT merupakan Liblary untuk memberikan token
const jwt = require('jsonwebtoken');

const User = require('../models/userModels');
// Funsi untuk mengubah email dan username menjadi capitalize
const Helpers = require('../Helpers/helper');
// Configurasi ke database mongoDb
const dbConfig = require('../config/secret');

module.exports = {
  async CreateUser(req, res) {
    const schema = Joi.object().keys({
      username: Joi.string()
        .min(5)
        .max(12)
        .required(),
      email: Joi.string()
        .email()
        .required(),
      password: Joi.string()
        .min(5)
        .max(12)
        .required()
    });
    // Validasi schema yang di buat
    const { error, value } = Joi.validate(req.body, schema);
    // Output validasi
    console.log(value);

    if (error && error.details) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        msg: error.details
      });
    }

    // Check the email is already exist
    const userEmail = await User.findOne({
      // this make sure the email is lower case by helper
      email: Helpers.lowerCase(req.body.email)
    });

    if (userEmail) {
      return res.status(HttpStatus.CONFLICT).json({
        message: 'Email already exist'
      });
    }

    // Check username is already exist
    const userName = await User.findOne({
      username: Helpers.firstUpper(req.body.username)
    });
    if (userName) {
      return res.status(HttpStatus.CONFLICT).json({
        message: 'Username already exist'
      });
    }

    // Encrypt the password
    return bcrypt.hash(value.password, 10, (err, hash) => {
      if (err) {
        return res.status(HttpStatus.BAD_REQUEST).json({
          message: 'Error hasing passowrd'
        });
      }
      const body = {
        username: Helpers.firstUpper(value.username),
        email: Helpers.lowerCase(value.email),
        password: hash
      };
      User.create(body)
        .then(user => {
          // set token
          const token = jwt.sign(
            {
              data: user
            },
            dbConfig.secret,
            {
              expiresIn: '1h'
            }
          );
          // res token
          res.cookie('auth', token);
          // response
          res.status(HttpStatus.CREATED).json({
            message: 'User create succesfully',
            user,
            token
          });
        })
        .catch(err => {
          res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
            message: err
          });
        });
    });
  },

  async LoginUser(req, res) {
    // Fungsi tidak boleh ada field yang kosong
    if (!req.body.username || !req.body.password) {
      return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
        message: 'No empty fields allowed'
      });
    }
    // Fungsi mencocokan login input dengan database berdasarkan username
    await User.findOne({
      username: Helpers.firstUpper(req.body.username)
    }).then(user => {
      // Jika username tidak ditemukan
      if (!user) {
        return res.status(HttpStatus.NOT_FOUND).json({
          message: 'Username not found'
        });
      }
      // Komparasi password dengan database
      return bcrypt
        .compare(req.body.password, user.password)
        .then(result => {
          if (!result) {
            return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
              message: 'Password is incorrect'
            });
          }
          const token = jwt.sign(
            {
              data: user
            },
            dbConfig.secret,
            {
              expiresIn: '1h'
            }
          );
          res.cookie('auth', token);
          return res.status(HttpStatus.OK).json({
            message: 'Login Successfull',
            user,
            token
          });
        })
        .catch(err => {
          return err.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
            message: 'Error occured'
          });
        });
    });
  }
};
