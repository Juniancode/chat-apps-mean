const Joi = require('joi');
const Post = require('../models/postModels');
const HttpStatus = require('http-status-codes');
const User = require('../models/userModels');

module.exports = {
  AddPost(req, res) {
    const schema = Joi.object().keys({
      post: Joi.string().required()
    });
    const { error } = Joi.validate(req.body, schema);
    if (error && error.details) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        msg: error.details
      });
    }
    const body = {
      user: req.user._id,
      username: req.user.username,
      post: req.body.post,
      created: new Date()
    };
    Post.create(body)
      .then(async post => {
        await User.update(
          {
            _id: req.user._id
          },
          {
            $push: {
              posts: {
                postId: post._id,
                post: req.body.post,
                ceated: new Date()
              }
            }
          }
        );
        res.status(HttpStatus.OK).json({
          message: 'post created',
          post
        });
      })
      .catch(err => {
        err.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
          message: ' Post failed Internal server error',
          err
        });
      });
  },
  async GetAllPosts(req, res) {
    try {
      const posts = await Post.find({})
        .populate('user')
        .sort({
          created: -1
        });
      return res.status(HttpStatus.OK).json({
        message: 'All Post',
        posts
      });
    } catch (error) {
      return error.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
        message: 'Error occured'
      });
    }
  },
  async AddLike(req, res) {
    const postId = req.body._id;
    await Post.update(
      {
        _id: postId,
        'likes.username': {
          $ne: req.user.username
        }
      },
      {
        $push: {
          likes: {
            username: req.user.username
          }
        },
        $inc: {
          totalLikes: 1
        }
      }
    )
      .then(() => {
        res.status(HttpStatus.OK).json({
          message: 'you like the post'
        });
      })
      .catch(error => {
        error.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
          message: 'Error occured'
        });
      });
  }
};
