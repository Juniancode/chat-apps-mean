const mongooose = require('mongoose');
const userSchema = mongooose.Schema({
  username: {
    type: String
  },
  email: {
    type: String
  },
  password: {
    type: String
  },
  posts: [
    {
      postId: {
        type: mongooose.Schema.Types.ObjectId,
        ref: 'Post'
      },
      post: {
        type: String
      },
      created: {
        type: Date,
        default: Date.now()
      }
    }
  ]
});

module.exports = mongooose.model('User', userSchema);
