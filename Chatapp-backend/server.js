const express = require('express');
const mongoose = require('mongoose');

// Call cookie parser
const cookieParser = require('cookie-parser');
// Call cors function
const cors = require('cors');
// call morgan
// const logger = require('morgan')
// Express
const app = express();
// Call configuration secret
const dbConfig = require('./config/secret');

// Import socket.io
const server = require('http').createServer(app);
const io = require('socket.io').listen(server);

app.use(cors());

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Credentials', 'true');
  res.header('Access-Control-Allow-Methodes', 'GET', 'POST', 'DELETE', 'PUT', 'OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Origin , X-Requested-With, Content-Type, Accept, Authorization');
  next();
});

app.use(
  express.json({
    limit: '50mb'
  })
);

app.use(
  express.urlencoded({
    extended: true,
    limit: '50mb'
  })
);

app.use(cookieParser());
// untuk menaruh cookie
// app.use(logger('dev'));
// untuk membuat log pada saat development

mongoose.Promise = global.Promise;

mongoose.connect(
  dbConfig.url,
  {
    useNewUrlParser: true
  }
);
require('./socket/streams')(io);
// Import auth function
const auth = require('./routes/authRoutes');
// Import post routes
const posts = require('./routes/postRoutes');
app.use('/api/chatapp', auth);
app.use('/api/chatapp', posts);

server.listen(3000, () => {
  console.log(' Running on port 3000');
});
